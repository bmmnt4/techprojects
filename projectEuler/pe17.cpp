// projectEuler17.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

int main()
{
	/************** psuedocode **************
	 * initialize numbers names to an array
	 * for each number 1 - 1000
	 *		disect the number
	 *		add the number of characters
	 * print answer
	 ****************************************/
	// null(0), one(3), two(3), three(5), four(4), five(4), six(3), seven(5), eight(5), nine(4)
	// null(0), ten*(3), twenty(6), thirty(6), forty(5), fifty(5), sixty(5), seventy(7), eighty(6), ninty(5)
	// null(0), one hundred and(13), two hundred and(13), three hundred and(15), four hundred and(14), five hundred and(14), six hundred and(13), seven hundred and(15), eight hundred and(15), nine hundred and(14)
	int wordLengths[3][10] = { { 0, 3, 3, 5, 4, 4, 3, 5, 5, 4 }, // ones Ex: one = 3 
								{ 0, 3, 6, 6, 5, 5, 5, 7, 6, 6 }, // tens Ex: ten = 3
								{ 0, 13, 13, 15, 14, 14, 13, 15, 15, 14 } }; // hundreds Ex: one hundred and = 13
	
	// ten(3), eleven-one(3), twelve-two(3), thirteen-three(3), fourteen-four(4), fifteen-five(3), sixteen-six(4), seventeen-seven(4), eighteen-eight(3), nineteen-nine(4)
	int tens[10] = { 3, 3, 3, 3, 4, 3, 4, 4, 3, 4 }; // compensation for tens like eleven
	int place = 0;
	int current = 0;
	int total = 0;
	int number = 0;
	for (int i = 1; i < 1000; i++)
	{ 
		number = i;
		place = 0;
		while (number > 0)
		{
			if (number % 10 == 1 && place == 1)
			{
				total += tens[current];
				current = number % 10;
			}
			else
			{
				current = number % 10;
				total += wordLengths[place][current];
			}
			number /= 10;
			place++;
		}
	}
	total += 11; // for one thousand
	total -= 3 * 9; // for extra ands
	cout << total << endl;
}

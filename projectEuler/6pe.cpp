/*****************************************************
 * Author: Brandon McCurry
 * Description: Solution to Project Euler problem 6
 *****************************************************/

#include <iostream>
using namespace std;

int main()
{
	long int sumOfSquares = 0;
	long int squareOfSums = 0;
	long int totalSum = 0;
	long int difference = 0;
	for (int i = 1; i <= 100; i++)
	{
		sumOfSquares += i * i;
		totalSum += i;
	}
	squareOfSums = totalSum * totalSum;
	difference = squareOfSums - sumOfSquares;
	cout << "squareOfSums: " << squareOfSums << endl;
	cout << "sumOfSquares: " << sumOfSquares << endl;
	cout << "difference: " << difference << endl;
}

/*****************************************************
 * Author: Brandon McCurry
 * Description: Solution to Project Euler problem 7
 *****************************************************/
#include <iostream>
#include <math.h>
#include <unistd.h>
using namespace std;

int main()
{
	int arraySize = 150000;
	int primes[arraySize];

	// initialize the array to all 0's
	for (int i = 0; i < arraySize; i++)
	{
		primes[i] = 0;
	}

	// create a table of primes by using multiples to eliminate non-primes
	int i = 2;
	int j = 2;
	primes[0] = 1;
	primes[1] = 1;
	while (i < arraySize)
	{
		if (j >= arraySize)
		{
			i++;
			j = i + i;
		}
		else
		{
			primes[j] = 1;
			j += i;
		}
	}

	// count the primes to find the 10,001st
	int h = 2;
	int counter = 0;
	while (h <= arraySize && counter <= 9999)
	{
		if (primes[h] == 0)
		{
			counter++;
		}
		h++;
	}
	cout << --h << endl;
}


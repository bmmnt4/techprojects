/******************************************************
 * Author: Brandon McCurry
 * Description: Solution to Project Euler problem 3
 ******************************************************/
#include <iostream>
using namespace std;

int main()
{
	// original too big, divided the original by the first prime that would evenly divide (71)
	long long unsigned NUM = 8462696833;
	int divisor = 2;
	
	while (NUM != divisor)
	{
	  if (NUM % divisor == 0)
		{
			NUM /= divisor;
		  divisor = 2;
		}
		else
		{
			divisor++;
		}
	}
	cout << divisor << endl;
}

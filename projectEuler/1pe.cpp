#include <iostream>
using namespace std;

int main()
{
	int M3 = 3;
	int M5 = 5;
	int total = 0;
	for (int i = M3; i < 1000; i+=M3)
	{
		total += i;
	}
	for (int i = M5; i < 1000; i+=M5)
	{
		if (i % 3 != 0)
		{
			total += i;
		}
	}
	cout << total << endl;
}

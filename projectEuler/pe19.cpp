/******************************
 * Author: Brandon McCurry
 * counts sundays on the first
 * of the month between 
 * 1/1/1901 to 12/31/2000
 ******************************/

/********** pseudocode **********
 * add 6 to calibrate to sunday
 * for 99 years
 *		add 30
 *		check if divisible by 7
 *		add 31
 *		check if divisible by 7
 *		...
 ********************************/

#include <iostream>
using namespace std;

int main()
{
	int sundays = 0;
	int days = 1;
	int dayOfWeek[] = { 0, 1, 0, 0, 0, 0, 0, 0 };
	int daysInMonth[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	for (int i = 1901; i < 2000; i++)
	{
		for (int j = 0; j < 12; j++)
		{
			dayOfWeek[days % 7] = 0;
			days += daysInMonth[j];
			if (i % 4 == 0 && j == 1)
			{
				days++;
			}
			dayOfWeek[days % 7] = 1;
			if (dayOfWeek[0] == 1)
			{
				sundays++;
			}
		}
	}
	cout << sundays << endl;
}        
/*
	int days = 6;
	int sundays = 0;
	for (int i = 0; i < 100; i++)
	{
		// Jan
		days += 31;
		if (days % 7 == 0)
			sundays += 1;
		
		// Feb
		days += 28;
		if (i % 4 == 0)
			days += 1;
		if (days % 7 == 0)
			sundays += 1;
		
		// Mar
		days += 31;
		if (days % 7 == 0)
			sundays += 1;
		
		// Apr
		days += 30;
		if (days % 7 == 0)
			sundays += 1;
		
		// May
		days += 31;
		if (days % 7 == 0)
			sundays += 1;
		
		// june
		days += 30;
		if (days % 7 == 0)
			sundays += 1;
		
		// july
		days += 31;
		if (days % 7 == 0)
			sundays += 1;
		
		// Aug
		days += 31;
		if (days % 7 == 0)
			sundays += 1;
		
		// Oct
		days += 30;
		if (days % 7 == 0)
			sundays += 1;
		
		// Sept
		days += 31;
		if (days % 7 == 0)
			sundays += 1;
		
		// Nov
		days += 30;
		if (days % 7 == 0)
			sundays += 1;
		
		// Dec
		days += 31;
		if (days % 7 == 0)
			sundays += 1;
	}
	cout << sundays;
	getchar();
}*/

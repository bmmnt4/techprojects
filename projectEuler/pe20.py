factorial = 1
sum = 0

for i in range(1, 100):
  factorial *= i

while factorial > 0:
  sum += factorial % 10
  factorial /= 10

print sum

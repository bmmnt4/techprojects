#include <iostream>
using namespace std;

int main()
{
	long long board[21][21];
	for (int i = 0; i < 21; i++)
	{
		for (int j = 0; j < 21; j++)
	{
			board[i][j] = 0;
		}
	}
	for (int i = 0; i < 21; i++)
	{
		for (int j = 0; j < 21; j++)
		{
			if ( i == 0 || j == 0)
			{
				board[i][j] = 1;
			}
			else
			{
				board[i][j] = board[i-1][j] + board[i][j-1];
			}
		}
	}
	for (int i = 0; i < 21; i++)
	{
		for (int j = 0; j < 21; j++)
		{
			cout << board[i][j] << " ";
		}
		cout << endl;
	}
}

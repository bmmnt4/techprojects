#include <iostream>
using namespace std;

int main()
{
	int num1 = 0;
	int num2 = 1;
	int num3 = 0;
	int total = 0;
	while (num3 < 4000000)
	{
		num3 = num1 + num2;
		num1 = num2;
		num2 = num3;
		
		if (num3 % 2 == 0)
		{
			total += num3;
		}
	}
	cout << "Total: " << total;
}
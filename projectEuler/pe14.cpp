#include <iostream>
using std::cout;
using std::endl;

int main()
{
	int n = 999999;
	int largest = 1;
	int large = n;
	while(n > 3)
	{
		int c = 1;
		long long num = n;
		while(num > 1)
		{
			if(num % 2 == 0)
			{
				num /= 2;
			}
			else
			{
				num = (3*num)+1;
			}
			c++;
		}
		if(c > largest)
		{
			cout << n << ": " << c << endl;
			largest = c;
			large = n;
		}
		n -= 2;
	}
	cout << large << ": " << largest << endl;
}
# TechProjects
This is a collection of most of my previous projects. Most are completed, some are not. I began coding in middle school, so some projects may be poorly coded, offensive, broken, or otherwise out of date.

Most of my more current projects have been server related. Unfortunately, these projects are difficult to show off without potentially risking some security. Due to this complication, I have simply listed my accomplishments here. Please ask me about them!!!
*  Nextcloud (SSL encryption, custom domain, subdomains, RAID, group folders, multiple users)
*  Custom email domain (brandon@bmccurry.com, among other aliases, are using tutanota for now for security and traffic load reasons.)
*  Plex server
*  Personal website: https://bmccurry.com (SSL encryption, robots.txt, contact form with spam prevention, docker)
*  Reverse proxied website: (hosting my senior design project in a VM on a separate computer, website using a separate port through docker)
*  OpenVPN
*  local DNS
*  Conky 
*  MSMTP (emails me when my UPS or Nextcloud server go down, or when I receive a message from the contact form)

Some of my projects in progress are:
*  Command center app (Server/Client to control certain aspects of my Linux desktop from my phone)
*  Nextcloud Backups (Having trouble verifying the backup integrity due to hard disk limitations)
*  Server virtualization (Working on Kubernetes now, waiting for new hardware for Promox)
*  Projects page of my website.
*  Some other automation that pops up as needed.
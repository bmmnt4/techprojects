import random

print("Welcome to my guessing game.")
game = "yes"
while game == "yes" or game == "y" or game == "Yes":
    answer = random.randint(0,100)
    guess = input("Guess a number between 1 and 100: ")
    guessing = True
    counter = 1
    valid = True
    while guessing:
        try:
            guess = int(guess)
            valid = True
        except:
            print("Invalid input\n")
            guess = input("Try again: ")
            valid = False
            
        if valid:
            if guess > 100 or guess < 0:
                print("Invalid input\n")
            else:
                if guess == answer:
                    guessing = False
                    print("Congradulations! You got it in " + str(counter) + " guesses!")
                elif guess > answer:
                    guess = input("Too high, Try again: ")
                elif guess < answer:
                    guess = input("Too low, Try again: ")
                counter += 1

    game = input("\nDo you want to play again: ")

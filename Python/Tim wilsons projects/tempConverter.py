#example only works with python3, in regular python an error occurs with res input not being a string


loop = True
#input value of Fahrenheit = 1, Celsius = -1
tempType = 1
FtoC = "Enter a value to be converted from Fahrenheit to Celsius\nTo convert from Celsius to Fahrenheit type 'switch'"
CtoF = "Enter a value to be converted from Celsius to Fahrenheit\nTo convert from Fahrenheit to Celsius type 'switch'"
while loop:
    loop = False
    #Fahrenheit is being entered
    if tempType > 0:
        print(FtoC)
    #Celsius is being entered
    else:
        print(CtoF)
    temp = input()
    if temp == "switch":
        print()
        tempType *= -1
        #Fahrenheit is being entered
        if tempType > 0:
            print(FtoC)
        #Celsius is being entered
        else:
            print(CtoF)
        temp = input()
    try:
        temp = int(temp)
        #Fahrenheit is being entered
        if tempType > 0:
            temp = (temp - 32)/1.8
        #Celsius is being entered
        else:
            temp = temp * 1.8 + 32

        print(temp)
        res = input("Would you like to add another?")
        if res == "yes" or res == "y" or res == "Yes":
            loop = True
    except:
        print("Invalid input")
        loop = True
        
    print()

        

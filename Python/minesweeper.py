# Author: Brandon McCurry
# Date: sometime in 2013-2014
# Description: I was learning tKinter for a class, and did this on the side as a result of my love for minesweeper.
#              I am aware that these graphics suck, but they are the best I knew how to do at the time. 


from tkinter import *
from random import randint
import time

class main(Frame):
    def __init__(self, master):
        super(main, self).__init__(master)
        self.grid()
        self.cur = 0
        self.rows = 10
        minesPlaced = 0
        self.columns = 10
        self.gameOver = False
        l=0
        self.mineNum = self.rows*self.columns/10
        self.destroyed = self.rows*self.columns
        self.flagged = int(self.mineNum)
        self.gameOverLbl = Label(self, text = "Game Over")
        self.timer = Label(self, text = "00")
        self.timer.grid(row = self.rows + 1, column = 0)
        self.restart = Button(self, text = "restart", command = self.reset)
        self.restart.grid(row = self.rows+1, column = int(self.columns/2-1), columnspan = 2)
        self.minesLeft = Label(self, text = self.flagged)
        self.minesLeft.grid(row = self.rows + 1, column = self.columns - 1)
        self.bttn = []
        self.field = []
        self.flagPos = []
        self.hintList = []
        self.update_clock()
            
        for r in range(self.rows):
            for c in range(self.columns):
                self.field.append(0)
                self.flagPos.append(0)
                self.button = Button(self, height = 1, width = 2, bg = "blue")
                self.button.bind("<Button-1>", lambda event, l=l: self.checkLoc(l))
                self.button.bind("<Button-3>", lambda event, l=l: self.flag(l))
                self.button.grid(row = r, column = c)
                self.bttn.append(self.button)
                l += 1

        while not minesPlaced >= self.mineNum:
            loc = randint(0, self.rows*self.columns-1)
            if not self.field[loc] == 1:
                self.field[loc] = 1
                minesPlaced += 1

    def flag(self, l):
        row = int(l/self.rows)
        col = l%self.columns
        if self.flagPos[l] == 1:
            self.bttn[l].configure(bg = "blue")
            self.flagged += 1
            self.flagPos[l] = 0
        else:
            self.bttn[l].configure(bg = "red")
            self.flagged -= 1
            self.flagPos[l] = 1
            
        self.minesLeft.configure(text = self.flagged)
        
    def checkLoc(self, l):
        if not self.gameOver:
            row = int(l/self.rows)
            col = l%self.columns 
            minesNear = 0
            if self.field[l] == 1:
                self.gameOverLbl.grid(columnspan = 3)
                self.gameOver = True
            elif not self.flagPos[l] == 1:
                self.bttn[l].destroy()
                self.destroyed -= 1
                for r in [row-1, row, row+1]:
                    for c in [col-1, col, col+1]:
                        if c < self.columns and r < self.rows and r >= 0 and c >= 0:
                            location = r*10+c
                            if self.field[location] == 1:
                                minesNear += 1

                self.hints = Label(self, text = minesNear, width = 2, height = 1)
                self.hints.grid(row = row, column = col)
                self.hintList.append(self.hints)
                if self.destroyed == self.mineNum:
                    self.gameOver = True
                    self.gameOverLbl.configure(text = "You win!")
                    self.gameOverLbl.grid(columnspan = 3)
            

    def update_clock(self):
        if not self.gameOver:
            self.cur += 1
            self.timer.configure(text = str(self.cur))
            self.after(1000, self.update_clock)

    def reset(self):
        self.destroy()
        app = main(root)

    
root = Tk()
root.title("Minesweeper")
root.geometry("240x300")
app = main(root)
root.mainloop()

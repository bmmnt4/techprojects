# Author: Brandon McCurry
# Date: spring of 2014
# Description: This is a basic calculator simply to practice using tkinter

from tkinter import *

root = Tk()
root.title("Calculator")
root.geometry("250x425")

class calc(Frame):
    def __init__(self, master):
        super(calc, self).__init__(master)
        self.grid()
        
        self.curNum = ""
        self.numbers = []
        
        self.screen = Text(self, width = 30, height = 5)
        self.screen.grid(row = 0, column = 0, columnspan = 4)
        
        self.b1 = Button(self, text = "1", font = ("Times New Roman", 32))
        self.b1["command"] = lambda: self.entNum("1")
        self.b1.grid(row = 2, column = 0)

        self.b2 = Button(self, text = "2", font = ("Times New Roman", 32))
        self.b2["command"] = lambda: self.entNum("2")
        self.b2.grid(row = 2, column = 1)

        self.b3 = Button(self, text = "3", font = ("Times New Roman", 32))
        self.b3["command"] = lambda: self.entNum("3")
        self.b3.grid(row = 2, column = 2)

        self.b4 = Button(self, text = "4", font = ("Times New Roman", 32))
        self.b4["command"] = lambda: self.entNum("4")
        self.b4.grid(row = 3, column = 0)

        self.b5 = Button(self, text = "5", font = ("Times New Roman", 32))
        self.b5["command"] = lambda: self.entNum("5")
        self.b5.grid(row = 3, column = 1)

        self.b6 = Button(self, text = "6", font = ("Times New Roman", 32))
        self.b6["command"] = lambda: self.entNum("6")
        self.b6.grid(row = 3, column = 2)

        self.b7 = Button(self, text = "7", font = ("Times New Roman", 32))
        self.b7["command"] = lambda: self.entNum("7")
        self.b7.grid(row = 4, column = 0)

        self.b8 = Button(self, text = "8", font = ("Times New Roman", 32))
        self.b8["command"] = lambda: self.entNum("8")
        self.b8.grid(row = 4, column = 1)

        self.b9 = Button(self, text = "9", font = ("Times New Roman", 32))
        self.b9["command"] = lambda: self.entNum("9")
        self.b9.grid(row = 4, column = 2)

        self.plus = Button(self, text = "+", font = ("Times New Roman", 32))
        self.plus["command"] = lambda: self.operation("add")
        self.plus.grid(row = 1, column = 0)

        self.minus = Button(self, text = "- ", font = ("Times New Roman", 32))
        self.minus["command"] = lambda: self.operation("min")
        self.minus.grid(row = 1, column = 1)

        self.mult = Button(self, text = "*", font = ("Times New Roman", 32))
        self.mult["command"] = lambda: self.operation("mult")
        self.mult.grid(row = 1, column = 2)

        self.div = Button(self, text = " / ", font = ("Times New Roman", 32))
        self.div["command"] = lambda: self.operation("divi")
        self.div.grid(row = 1, column = 3)

        self.equals = Button(self, text = "= ", font = ("Times New Roman", 32))
        self.equals["command"] = lambda: self.operation("equals")
        self.equals.grid(row = 4, column = 3)

        self.clear = Button(self, text = "clr", command = self.clear, font = ("Times New Roman", 30))
        self.clear.grid(row = 3, column = 3)

        self.dec = Button(self, text = " . ", font = ("Times New Roman", 32))
        self.dec["command"] = lambda: self.entNum(".")
        self.dec.grid(row = 2, column = 3)

    def entNum(self, num):
        self.curNum += num
        self.screen.delete(0.0, END)
        self.screen.insert(0.0, self.curNum)

    def operation(self, op):
        self.numbers.append(self.curNum)
        self.curNum = ""
        self.combine()
        self.add = False
        self.min = False
        self.multi = False
        self.divi = False
        if op == "add":
            self.add = True
        elif op == "min":
            self.min = True
        elif op == "mult":
            self.multi = True
        elif op == "divi":
            self.divi = True
        elif op == "equals":
            self.screen.insert(0.0, self.numbers[0])

    def clear(self):
        self.numbers = []
        self.curNum = ""
        self.screen.delete(0.0, END)

    def combine(self):
        try:
            self.screen.delete(0.0, END)
            if not self.numbers[1] == None:
                if self.add:
                    self.numbers[0] = float(self.numbers[0]) + float(self.numbers[1])
                elif self.min:
                    self.numbers[0] = float(self.numbers[0]) - float(self.numbers[1])
                elif self.multi:
                    self.numbers[0] = float(self.numbers[0]) * float(self.numbers[1])
                elif self.divi:
                    self.numbers[0] = float(self.numbers[0]) / float(self.numbers[1])
                self.numbers.pop(1)
        except:
            pass
        

app = calc(root)
root.mainloop()

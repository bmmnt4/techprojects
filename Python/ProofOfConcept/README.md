As the folder name suggests, these are all simply proof of concept programs.

realTime.py is simply a clock and nothing else.

people.py is a simple database for customers. It has a buggy UI because I was learning databases, and didn't care enough to fix the GUI.

snake.py is the game snake but it waits for your input before each step, because I could never figure out how to do it properly in python.

crawler.py is a simple web crawler.

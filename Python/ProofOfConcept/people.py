#!/usr/bin/python3
import pickle
from tkinter import *

root = Tk()
root.title("SSN Search")
root.geometry("400x200")

class SSN_Search(Frame):
    def __init__(self, master):
        self.creating = False
        self.info = False
        super(SSN_Search, self).__init__(master)
        self.grid()
        
        self.SSNLbl = Label(self, text = "Social Security Number")
        self.SSNLbl.grid(row = 0, column = 0, columnspan = 2)

        self.SSNEntry = Text(self, width = 10, height = 1)
        self.SSNEntry.insert(0.0, "SSN")
        self.SSNEntry.grid(row = 1, column = 0)

        self.search = Button(self, text = "Submit", command = self.search)
        self.search.grid(row = 1, column = 1)

    def create(self):
        if self.info:
            self.SSlbl.destroy()
            self.info = False
            
        self.creating = True
        self.first = Text(self, width = 10, height = 1)
        self.first.insert(0.0, "First")
        self.first.grid(row = 2, column = 0)

        self.last = Text(self, width = 10, height = 1)
        self.last.insert(0.0, "Last")
        self.last.grid(row = 2, column = 1)

        self.address = Text(self, width = 10, height = 1)
        self.address.insert(0.0, "address")
        self.address.grid(row = 3, column = 0)

        self.city = Text(self, width = 10, height = 1)
        self.city.insert(0.0, "city")
        self.city.grid(row = 4, column = 0)

        self.state = Text(self, width = 10, height = 1)
        self.state.insert(0.0, "state")
        self.state.grid(row = 4, column = 1)

        self.zip = Text(self, width = 10, height = 1)
        self.zip.insert(0.0, "zip")
        self.zip.grid(row = 4, column = 2)

        self.infoFail = Label(self, text = "Number not found. Please enter information.")
        self.infoFail.grid(row = 5, column = 0, columnspan = 5)

        self.submit = Button(self, text = "Submit", command = self.addUser)
        self.submit.grid(row = 6, column = 0)

    def addUser(self):
        self.entries[self.SS] = str(self.first.get(0.0, END))
        self.entries[self.SS] += str(self.last.get(0.0, END))
        self.entries[self.SS] += str(self.address.get(0.0, END))
        self.entries[self.SS] += str(self.city.get(0.0, END))
        self.entries[self.SS] += str(self.state.get(0.0,END))
        self.entries[self.SS] += str(self.zip.get(0.0, END))
        self.f = open("people.dat", "wb")
        pickle.dump(self.entries, self.f)
        self.f.close()
        self.f = open("people.dat", "rb")
        self.entries = pickle.load(self.f)
        self.f.close()
        self.remove_create()

    def search(self):
        try:
            self.f = open("people.dat", "rb")
            self.entries = pickle.load(self.f)
            self.f.close()
        except:
            self.f = open("people.dat", "wb")
            empty = {}
            pickle.dump(empty, self.f)
            self.f.close()
            self.f = open("people.dat", "rb")
            self.entries = pickle.load(self.f)
            self.f.close()

        self.SS = self.SSNEntry.get(0.0, END)
        self.remove_create()
        
    def remove_create(self):
        if self.SS in self.entries:
            if self.creating:
                self.first.destroy()
                self.last.destroy()
                self.address.destroy()
                self.city.destroy()
                self.state.destroy()
                self.zip.destroy()
                self.submit.destroy()
                self.infoFail.destroy()
                self.creating = False
            
            self.SSlbl = Label(self, text = self.entries[self.SS])
            self.SSlbl.grid(row = 2, column = 0)
            self.info = True
        else:
            self.create()

app = SSN_Search(root)
root.mainloop()

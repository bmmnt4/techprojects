from random import randint

class snake(object):
    def __init__(self):
        self.__length = 1
        self.snakeBoard = [
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0]]
        
        self.col = randint(1,9)
        self.row = randint(1,9)
        self.tailRow = self.row
        self.tailCol = self.col
        self.snakeBoard[self.row][self.col] = 1
        
    def placeFood(self):
        self.__foodRow = randint(1,9)
        self.__foodCol = randint(1,9)
        if(self.snakeBoard[self.__foodRow][self.__foodCol] == 0):
            self.snakeBoard[self.__foodRow][self.__foodCol] = -1
        else:
            self.placeFood()
        

    def display(self):
        for i in self.snakeBoard:
            print(i)

    def playSnake(self):
        self.placeFood()
        theRow = self.row
        theCol = self.col
        end = "false"
        while end != "true":
            self.display()
            key = input()
            if key == "w":
                theRow -= 1
            elif key == "a":
                theCol -= 1
            elif key == "s":
                theRow +=1
            elif key == "d":
                theCol += 1
            try:
                if self.snakeBoard[theRow][theCol] < 0:#if -1 place new food
                    self.placeFood()
                    self.__length += 1
                else:
                    c = -1
                    for a in self.snakeBoard:
                        d = -1
                        c += 1
                        for b in a:
                            d+= 1
                            if(b > 0):
                                self.snakeBoard[c][d] = b-1
                if self.snakeBoard[theRow][theCol] > 0:
                    end = "true"
                self.row = theRow
                self.col = theCol
                self.snakeBoard[self.row][self.col] = self.__length
            except:
                end = "true"
            
one = snake()
one.playSnake()

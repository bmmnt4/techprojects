#!/usr/bin
import urllib2
from lxml import html
import requests
import time

links = ['http://www.google.com']
count = 0
for link in links:
    time.sleep(1)
    response = urllib2.urlopen(link)
    html = response.read()
    for word in html.split('\"'):
        if ".com" in word:
            if word[0:5] == "http:" or word[0:6] == "https:":
                if word not in links:
                    links.append(word)
                    print(word)

"""
scan html for specific sections
read URLS to skip some
remove scanned urls from links
"""

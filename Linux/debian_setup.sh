#!/bin/bash

# Author: Brandon MCCurry
# Description: I created this script in 2015 when first getting into Linux. 
#              The purpose of this script was to quickly install the basics to a fresh install.

MACHINE_TYPE=`uname -m`
if [ ${MACHINE_TYPE} == 'x86_64' ]; then
  cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
else
  cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86" | tar xzf -
fi
~/.dropbox-dist/dropboxd
sudo apt-get install chromium xfce4-goodies g++ gedit scite codeblocks idle3 ark
xrandr --output DVI-I-1 --left-of DVI-I-2
exit 1

#!/bin/bash

# I created a button in the xfce-panel, and eventually a keyboard shortcut, which called this script in order to lock the screen.
xscreensaver-command -lock
xset dpms force standby

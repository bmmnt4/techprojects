#!/bin/bash

# The brightness keys on my laptop do not work. When pressed, they lower the 
# screen to the dimmest setting and can only be fixed from the bios. 
# This was my attempt to work around that issue.

vol=$(cat /sys/class/backlight/ideapad/brightness)
let "vol -= 1"
echo $vol > /sys/class/backlight/ideapad/brightness

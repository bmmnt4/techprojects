#!/bin/bash

# This script is poorly, but effectively, written to update my system in a single command. I have an alias ("update") setup to execute this script.
# I know that having -y appended to the end of each of these commands is dangerous, but I included it because I am lazy and it hasn't failed me yet. I would not do this in a professional setting, only home machines.

logFile=/home/brandon/.config/conky/Messages/NZXTlastUpdate.txt

apt-get update
apt-get dist-upgrade -y
apt-get upgrade -y
apt-get --fix-missing install -y
apt-get --fix-broken install -y
apt-get autoremove -y

sed -i '1s=.*='"$(date '+%D %T')"'=g' "$logFile"

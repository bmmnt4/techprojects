#!/bin/bash
#This file is meant for the host machine
#	Description: reports some stats from the UPS and from PiVPN which require root permissions to run
#              there is probably a better way to do this, but for now this works

#need to come back and do some error checking
#check for the file
#create the file if it doesn't exist
logFile=/home/brandon/.config/conky/Messages/batteryLog.txt

BATTERY_LOAD=$(echo | pwrstat -status | grep Load | cut --complement -d '(' -f 1 | cut -d ' ' -f 1 | awk '{$1=$1};1')
BATTERY_CAPACITY=$(echo | pwrstat -status | grep Capacity | sed -r 's/[^0-9]+//g')
REMAINING_RUNTIME=$(echo | pwrstat -status | grep Runtime | sed -r 's/[^0-9]+//g')
VPN_STATUS=$(nmap -sU -p 11981 192.168.0.14 | egrep -o [a-z]+[\|]+[a-z]+)
sudo -u brandon sed -i -r '1s/[0-9]+/'"$BATTERY_LOAD"'/g' "$logFile"
sudo -u brandon sed -i -r '2s/[0-9]+/'"$BATTERY_CAPACITY"'/g' "$logFile"
sudo -u brandon sed -i -r '3s/[0-9]+/'"$REMAINING_RUNTIME"'/g' "$logFile"
sudo -u brandon sed -i -r '4s/[a-z]+[\|]?[a-z]+/'"$VPN_STATUS"'/g' "$logFile"

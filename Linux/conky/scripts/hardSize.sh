#!/bin/bash
#This file is meant for the server machine
#	Description: This file reports the servers storage space, uptime, and CPU usage
#							 The CPU is not being reported at the moment due to unresolved errors

logFile=/home/bmmnt4/Documents/conky/Messages/stats.log

usedSize=$(df /dev/mapper/xubuntu--vg-root -h --output=used | egrep '[0-9]+G' | awk '{$1=$1};1')
totalSize=$(df /dev/mapper/xubuntu--vg-root -h --output=size | egrep '[0-9]+G' | awk '{$1=$1};1')

uptimeDays=$(uptime | egrep -o '[0-9]+\sdays' | egrep -o '[0-9]+')
uptimeHours=$(uptime -p | egrep -o '[0-9]+\shours' | egrep -o '[0-9]+')
uptimeMins=$(uptime -p | egrep -o '[0-9]+\smin' | egrep -o '[0-9]+')

if [[ $uptimeDays == "" ]]; then
	uptimeDays=0
fi
if [[ $uptimeHours == "" ]]; then
	uptimeHours=0
fi
if [[ $uptimeMins == "" ]]; then
	uptimeMins=0
fi

cpuPerc=0
#cpuPerc=$(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage "%"}')

if [ ! -f "$logFile" ]; then
	echo "ideaPad   " $usedSize "/" $totalSize > "$logFile"
	echo "ideaPad" $uptimeDays"d:"$uptimeHours"h:"$uptimeMins"m" >> "$logFile"
	echo "ideaPad" $cpuPerc% >> "$logFile"
	echo "ideaPad never1">> "$logFile"
else
	sed -i -r '1s/[0-9]+G\s\/\s[0-9]+G/'"$usedSize"' \/ '"$totalSize"'/g' "$logFile"
	sed -i -r '2s/.*/ideaPad '"$uptimeDays"'d:'"$uptimeHours"'h:'"$uptimeMins"'m/' "$logFile"
	sed -i -r '3s/[0-9]+/'"$cpuPerc"'/g' "$logFile"
fi


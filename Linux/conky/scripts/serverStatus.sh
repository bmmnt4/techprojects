#!/bin/bash
#This file is meant for the host machine
# Description: reports server status for Nextcloud, NordVPN, and PiVPN to a logFile
#							 also emails me if Nextcloud is down

From=notifydaemon282@gmail.com
To=localtuxian@bmccurry.com
Subject="ERROR: Nextcloud offline!!!!"
Body="Nextcloud offline"
Server=smtp.googlemail.com:587
Username=notifydaemon282
Password=bmccurry

logFile=/home/brandon/.config/conky/Messages/serverStatus.log

serverResponse=$(curl -s -w "%{http_code}\n" -L http://brandonmccurry.ddns.net/nextcloud/index.php/login/ -o /dev/null)
service="Nextcloud status: "
statusResponse=$(cat /home/brandon/.config/conky/Messages/serverStatus.log | grep Nextcloud | sed -r 's/'"$service"'//g')
if [[ $serverResponse == 200 || $serverResponse == 403 ]]
then
	sed -i.bak -r 's/'"$service"'[a-zA-Z]+/'"$service"'Up/g' "$logFile"
elif [[ $statusResponse == "Up" ]]
then
	sed -i.bak -r 's/'"$service"'[a-zA-Z]+/'"$service"'Pending/g' "$logFile"
elif [[ $statusResponse == "Pending" ]]
then
	sed -i.bak -r 's/'"$service"'[a-zA-Z]+/'"$service"'Down/g' "$logFile"
	sendemail -f $From -t $To -m $Body -u $Subject -s $Server -xu $Username -xp $Password
else 
	sed -i.bak -r 's/'"$service"'[a-zA-Z]+/'"$service"'Error/g' "$logFile"
fi

echo "test" >> /home/brandon/testting.txt 
serverResponse=$(nordvpn status | grep Status | sed 's/Status: //g')
sed -i.bak -r 's/NordVPN status:.*/NordVPN status: '"$serverResponse"'/g' "$logFile"

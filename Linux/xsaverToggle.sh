#!/bin/bash

# I created a toggle button in the xfce4-panel to enable and disable the screensaver 
# so that it would not lock while I was watching a show.

if [[ "$(pidof xscreensaver)" ]]
then
	xscreensaver-command -exit
	notify-send "Screensaver disabled"
	cp /home/brandon/Dropbox/Pictures/switch-off.png /home/brandon/Dropbox/Pictures/switch.png
else 
	xscreensaver -no-splash &
	notify-send "Screensaver enabled"
	cp /home/brandon/Dropbox/Pictures/switch-on.png /home/brandon/Dropbox/Pictures/switch.png
fi
xfce4-panel -r

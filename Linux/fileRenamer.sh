#!/bin/bash

# Author: Brandon McCurry
# Description: I had extracted a lot of songs from an old iPod, but the naming convention was four random digits.
#              This script uses mediainfo to rename the files to the song name, and place them in a folder named after the artist/band.

PWD=/home/brandon/Nextcloud/Music/Itunes

for i in $PWD/*.m4*;
do
	ext=$(echo $i | cut -d '.' -f 2)
	track=$(echo | mediainfo "$i" | grep "Track name " | cut -d ':' -f 2 | awk '{$1=$1};1' | sed -n 1p)
	artist=$(echo | mediainfo "$i" | grep "Performer" | cut -d ':' -f 2 | awk '{$1=$1};1' | sed -n 1p)
	newDir=$(echo "$PWD"/"$artist")
	if [ ! -d "$newDir" ]; then
		mkdir "$newDir"
	fi
	newFile=$(echo "$newDir"/"$track.$ext")
	echo "$newFile"
	cp "$i" "$newFile"
done



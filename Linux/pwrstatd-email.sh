#!/bin/bash

#This script is setup to send me an email when my power goes out and my UPS activates.

From=notifydaemon@something.com
To=username@something.com
Subject="ERROR: power failure!!"
Body="Power failure"
Server=smtp.googlemail.com:587
Username=username
Password=password
Date=`date +'%Y/%m/%d %p %H:%M'`

RUNTIME=""

if [ $REMAINING_RUNTIME != none ]; then
	RUNTIME="Remaining Runtime: $REMAINING_RUNTIME Seconds"
fi

DATA=(
"==================================================="
"	$Subject																					"
"==================================================="
"\n"
"\n"
"$Body\n"
"Time: $Date\n"
"UPS Model Name: $MODEL_NAME\n"
"Battery Capacity: $BATTERY_CAPACITY %\n"
"$RUNTIME\n"
)
Message=$(echo "${DATA[*]}")
sendemail -f $From -t $To -m $Message -u $Subject -s $Server -xu $Username -xp $Password

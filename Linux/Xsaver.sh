#!/bin/bash

# Way back when, maybe still, Linux would lock the screen for inactivity while I was watching a show. 
# In order to prevent this, I wrote this script to disable the screensaver, then re-enable it after a set duration.

xscreensaver-command -exit
sleep $1
xscreensaver &

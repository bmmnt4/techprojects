/*****************************************************
 * Author: Brandon McCurry
 * Date: around sometime in 2014
 * Description: Converts user input into binary
 *              This is the first C++ program I wrote
 *****************************************************/

#include <iostream>
#include <string>

using namespace std;
int binary(char);

int main(void)
{
	string word;
	cout << "Please enter a string to encode: " << endl;
	getline(cin, word);
	int size = word.size();
	for(int l = 0; l < size; l++)
	{
		binary(word[l]);
	}
	cout << endl;
}
int binary(char letter)
{
	char binary[8];
	int num = 128;
	int i = 0;
	int code = static_cast< int >(letter);
	while(num >= 1)
	{   
		if(code >= num)
		{
			binary[i] = 1;
			code -= num;
		}
		else
		{
			binary[i] = 0;
		}
		num /= 2;
		int blah = binary[i];
		cout << blah;
		i++;
	}
	cout << " ";
	return 0;
}



#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main()
{
	string BASE_FILE = "letters";
  ifstream wordFile;
	string wordList = "";
	string scramWord = "";
	string matchedWord;
	string fileWord = "";
	string SCRAMWORD = "";
	int j;
	bool match = true;
  cin >> SCRAMWORD;
	cout << endl << endl;
	while (SCRAMWORD != ".") // for each input word
	{
		wordList = "words.txt";//BASE_FILE + static_cast<char>(SCRAMWORD.length() + 48) + ".txt";
		wordFile.open(wordList.c_str());
		while(wordFile)// && !match) // check every word in the file until a match is found
		{
			match = true;
			wordFile >> fileWord;  // take input from file
			scramWord = SCRAMWORD;
			for (int i = 0; i < fileWord.length() && match; i++) // check each letter in the word from file
			{
				match = false;
			  j = 0;	// look for matching letter in input word
				while (j < fileWord.length() && fileWord[i] != scramWord[j])
				{
					j++;
				}
				if (scramWord[j] == fileWord[i])
				{
					scramWord[j] = ' ';
					match = true;
				}
			}
			if (match)
			{
				matchedWord = fileWord;
			}
		}
		cout << matchedWord << ",";
		wordFile.close();
		cin >> SCRAMWORD;
	}
}

#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main()
{
  ifstream wordList;
	string word = "";
	string BASE_FILE = "letters";
	string outputFile = BASE_FILE + ".txt";
	ofstream sorted;
  wordList.open("words.txt");
  while (wordList)
	{
		wordList >> word;
		outputFile = BASE_FILE + static_cast<char>(word.length() + 48) + ".txt";
		sorted.open(outputFile.c_str(), ofstream::out | ofstream::app);
	  sorted << word << endl;
		sorted.close();
	}
	wordList.close();
}

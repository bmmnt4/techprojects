#include <stdio.h>
#include <ctype.h>
#include <cs50.h>
#include <string.h>

/*************************
 * uses vigenere to 
 * encrypt a given phrase
 *************************/

int main(int argc, char* argv[])
{
    // if arguments are valid run program
    if (argc == 2)
    {
        // make sure argument characters are valid
        for (int i = 0; i < strlen(argv[1]); i++)
        {
            if (!isalpha(argv[1][i]))
            {
                printf("Invalid Characters");
                return 1;
            }
        }
        string plain = GetString();
        int lowerKey = 'z';
        int lower = 'a';
        char new = 'a';
        string key = argv[1];
        int len = strlen(key);
        // add each letter by its corresponding key letter
        for (int i = 0, k = 0; i < strlen(plain); i++)
        {
            if (isalpha(plain[i]))
            {
                // ignore key capitalization
                lowerKey = tolower(key[k % len]) - lower;
                // maintain cases
                if (islower(plain[i]))
                {
                    new = (plain[i] + lowerKey - 97) % 26 + 97;
                }
                else
                {
                    new = (plain[i] + lowerKey - 64) % 26 + 64;
                }
                k++;
            }
            else
            {
                new = plain[i];
            }
            printf("%c", new);
        }
        printf("\n");
        return 0;
    }
    // if arguments are invalid yell at user
    else
    {
        printf("Missing parameters\n");
        return 1;
    }
}

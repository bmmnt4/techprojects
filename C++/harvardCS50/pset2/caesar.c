#include <stdio.h>
#include <string.h>
#include <cs50.h>
#include <ctype.h>

/****************************
 * prints the given phrase
 * after encrypting with key
 * entered via argument
 ****************************/
int main(int argc, char* argv[])
{
    // if arguments are proper run the program
    if (argc == 2 || isdigit(argv[1]))
    {
        // atoi turns the char argument into an int
        int key = atoi(argv[1]);
        char new = 'a';
        // get a phrase from the user to encrypt
        string plain = GetString();
        
        // add each letter by the key value
        for (int i = 0; i < strlen(plain); i++)
        {
            // only change characters in the alphabet
            if (isalpha(plain[i]))
            {
                // maintain case 
                if (islower(plain[i]))
                {
                    new = (plain[i] + key - 97) % 26 + 97;
                }
                else
                {
                    new = (plain[i] + key - 64) % 26 + 64;
                }
            }
            else
            {
                new = plain[i];
            }
            printf("%c", new);
        }
        printf("\n");
        return 0;
    }
    // yell at user for improper arguments
    else
    {
        printf("Missing arguements\n");
        return 1;
    }
}

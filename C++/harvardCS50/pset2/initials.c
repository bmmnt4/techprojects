#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>
/***********************
 * Takes in a name,
 * returns the initials
 ***********************/
int main(void)
{
    char* name = GetString();
    // prints the first initial capitalized
    printf("%c", toupper(name[0]));
    // prints each letter capitalized that follows a space
    for(int i = 0; i < strlen(name); i++)
    {
        if (name[i] == ' ')
        {
            printf("%c", toupper(name[i + 1]));
        }
    }
    printf("\n");
}

#include <stdio.h>
#include <cs50.h>
#include <math.h>

/**************************
 * gets the proper amount
 * of change using the
 * fewest coins possible
 **************************/
 
int main(void)
{
    float change;
    // ask how much change is owed
    do
    {
        printf("O hai! How much change is owed?\n");
        change = GetFloat();
    }while(change < 0.00);
    
    // get proper change
    int owed = round(change * 100);
    int count = 0;
    
    int quarters = owed / 25.0;
    owed -= quarters * 25;
    
    int dimes = owed / 10.0;
    owed -= dimes * 10;
    
    int nickles = owed / 5.0;
    owed -= nickles * 5;
    
    int pennies = owed / 1.0;
    owed -= pennies;
    
    // print number of coins
    count = quarters + dimes + nickles + pennies;    
    printf("%i\n", count);
}
    

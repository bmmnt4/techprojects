#include <stdio.h>
#include <cs50.h>

/**********************
 * prints mario steps
 **********************/

int main(void)
{
    int rows;
    // ask until user inputs height between 23 and 0
    do
    {
        printf("Height: ");
        rows = GetInt();
    } while (rows > 23 || rows < 0);
    
    // design pyramid
    for (int i = 0; i < rows; i++)
    {
        int spaces = rows - i - 1;
        for (int j = 0; j < rows + 1; j++)
        {
            if (j < spaces)
            {
                printf(" ");
            }
            else
            {
                printf("#");
            }
        }
        printf("\n");
    }            
}

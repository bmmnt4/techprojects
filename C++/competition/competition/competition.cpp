/******************************
 * Author: Brandon McCurry
 * Decimal to Binary, 
 * AND Binary to Decimal
 ******************************/

/****************** pseudocode ******************
 * Ask user conversion direction
 * if decimal to binary
 *		ask for a decimal
 *		while decimal > 0
 *			place decimal % 2 at the beginning
 *		print answer
 * if binary to decimal
 *		ask for binary
 *		while binary > 0
 *			get % 10 * the position
 *			change the position
 *		print answer
 * if exit 
 *		end
 ************************************************/

// total time = 1h 5m
//#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
	string response = "";
	cout << "What do you want to do (Dec, Bin, Quit): ";
	cin >> response;

	while (response != "Quit")
	{
		// converting decimal to binary
		// roughly an hour
		if (response == "Dec")
		{
			int decimal = 0;
			int binary = 0;
			int multiplier = 1;
			cout << "Please enter a decimal: ";
			cin >> decimal;
			// place the remainder at the beginning, then divide by 2
			while (decimal > 0)
			{
				binary += decimal % 2 * multiplier;
				multiplier *= 10;
				decimal /= 2;
			}
			cout << binary << endl;
		}

		// converting binary to decimal
		// time roughly 10 minutes
		else if (response == "Bin")
		{
			int binary = 0;
			int pos = 1;
			int decimal = 0;
			cout << "Please enter a binary number: ";
			cin >> binary;

			// add based on position
			while (binary > 0)
			{
				decimal += binary % 10 * pos;
				binary /= 10;
				pos += pos;
			}
			cout << decimal << endl;
		}
		cout << "What do you want to do (Dec, Bin, Quit): ";
		cin >> response;
	}
}


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settings.h"
#include <QTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
    QChar letter;
    do
    {
        letter = qrand()%(97+26-65) + 65;
    }while(letter > (65 + 26) && letter < 97);
    ui->character->setText(letter);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_submit_pressed()
{
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
    QString binaryEntry = ui->bin128->text() + ui->bin64->text() + ui->bin32->text()
            + ui->bin16->text() + ui->bin8->text() + ui->bin4->text()
            + ui->bin2->text() + ui->bin1->text();
    int decimal = ui->character->text().at(0).unicode();
    int translation = 0;
    QString converted = "";
    int multiplier = 1;
    // place the remainder at the beginning, then divide by 2
    while (decimal > 0)
    {
        translation += decimal % 2 * multiplier;
        multiplier *= 10;
        decimal /= 2;
    }
    if (translation < 1000000)
    {
        converted = "0" + QString::number(translation);
    }
    converted = "0" + QString::number(translation);
    if (converted == binaryEntry)
    {
        if (ui->clear->isChecked())
        {
            ui->bin1->setText("0");
            ui->bin2->setText("0");
            ui->bin4->setText("0");
            ui->bin8->setText("0");
            ui->bin16->setText("0");
            ui->bin32->setText("0");
            ui->bin64->setText("0");
            ui->bin128->setText("0");
        }
        QChar newChar;
        do
        {
            newChar = qrand()%(97+26-65) + 65;
        }while(newChar > (65 + 26) && newChar < 97);

        ui->character->setText(newChar);
        ui->confirmation->setText("Correct");
    }
    else
    {
        ui->confirmation->setText("Incorrect");
    }
}

/*void MainWindow::on_settings_pressed()
{
    Settings win;
    win.setModal(true);
    win.exec();
}*/

void MainWindow::on_bin128_pressed()
{
    ui->bin128->setText(QString::number(!ui->bin128->text().toInt()));
}

void MainWindow::on_bin64_pressed()
{
    ui->bin64->setText(QString::number(!ui->bin64->text().toInt()));
}

void MainWindow::on_bin32_pressed()
{
    ui->bin32->setText(QString::number(!ui->bin32->text().toInt()));
}

void MainWindow::on_bin16_pressed()
{
    ui->bin16->setText(QString::number(!ui->bin16->text().toInt()));
}

void MainWindow::on_bin8_pressed()
{
    ui->bin8->setText(QString::number(!ui->bin8->text().toInt()));
}

void MainWindow::on_bin4_pressed()
{
    ui->bin4->setText(QString::number(!ui->bin4->text().toInt()));
}

void MainWindow::on_bin2_pressed()
{
    ui->bin2->setText(QString::number(!ui->bin2->text().toInt()));
}

void MainWindow::on_bin1_pressed()
{
    ui->bin1->setText(QString::number(!ui->bin1->text().toInt()));
}



#ifndef ASCIIQUIZ_H
#define ASCIIQUIZ_H

#include <QMainWindow>

namespace Ui {
class ASCIIQuiz;
}

class ASCIIQuiz : public QMainWindow
{
    Q_OBJECT

public:
    explicit ASCIIQuiz(QWidget *parent = 0);
    ~ASCIIQuiz();

private slots:
    void on_bin128_pressed();

private:
    Ui::ASCIIQuiz *ui;
};

#endif // ASCIIQUIZ_H

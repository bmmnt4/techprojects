#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_submit_pressed();

    void on_bin128_pressed();

    void on_bin64_pressed();

    void on_bin32_pressed();

    void on_bin16_pressed();

    void on_bin8_pressed();

    void on_bin4_pressed();

    void on_bin2_pressed();

    void on_bin1_pressed();

    //void on_settings_pressed();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

#include "mainwindow.h"
#include <QApplication>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    /*QWidget *window = new QWidget;
    QHBoxLayout *lay1 = new QHBoxLayout;
    QHBoxLayout *lay2 = new QHBoxLayout;
    QHBoxLayout *lay3 = new QHBoxLayout;
    QHBoxLayout *lay4 = new QHBoxLayout;
    QHBoxLayout *lay5 = new QHBoxLayout;
    QHBoxLayout *lay6 = new QHBoxLayout;
    QHBoxLayout *lay7 = new QHBoxLayout;
    QVBoxLayout *vlay = new QVBoxLayout;

    QPushButton *settings = new QPushButton("settings");
    QLabel *character = new QLabel("A");
    QLabel *info = new QLabel("");
    QPushButton *bin128 = new QPushButton("0");
    QPushButton *bin64 = new QPushButton("0");
    QPushButton *bin32 = new QPushButton("0");
    QPushButton *bin16 = new QPushButton("0");
    QPushButton *bin8 = new QPushButton("0");
    QPushButton *bin4 = new QPushButton("0");
    QPushButton *bin2 = new QPushButton("0");
    QPushButton *bin1 = new QPushButton("0");
    QLabel *bit128 = new QLabel("128");
    QLabel *bit64 = new QLabel("64");
    QLabel *bit32 = new QLabel("32");
    QLabel *bit16 = new QLabel("16");
    QLabel *bit8 = new QLabel("8");
    QLabel *bit4 = new QLabel("4");
    QLabel *bit2 = new QLabel("2");
    QLabel *bit1 = new QLabel("1");
    QPushButton *submit = new QPushButton("Submit");
    QCheckBox *clear = new QCheckBox("Clear text on correct answer");

    lay1->addWidget(settings);
    lay2->addWidget(character);
    lay3->addWidget(info);
    lay4->addWidget(bin128);
    lay4->addWidget(bin64);
    lay4->addWidget(bin32);
    lay4->addWidget(bin16);
    lay4->addWidget(bin8);
    lay4->addWidget(bin4);
    lay4->addWidget(bin2);
    lay4->addWidget(bin1);
    lay5->addWidget(bit128);
    lay5->addWidget(bit64);
    lay5->addWidget(bit32);
    lay5->addWidget(bit16);
    lay5->addWidget(bit8);
    lay5->addWidget(bit4);
    lay5->addWidget(bit2);
    lay5->addWidget(bit1);
    lay6->addWidget(submit);
    lay7->addWidget(clear);

    vlay->addLayout(lay1);
    vlay->addLayout(lay2);
    vlay->addLayout(lay3);
    vlay->addLayout(lay4);
    vlay->addLayout(lay5);
    vlay->addLayout(lay6);
    vlay->addLayout(lay7);

    window->setLayout(vlay);

    window->show();
    */

    //w.showMaximized();
    w.show();

    return a.exec();
}

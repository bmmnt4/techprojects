#ifndef BUTTON1POPUP_H
#define BUTTON1POPUP_H

#include <QDialog>

namespace Ui {
class Button1Popup;
}

class Button1Popup : public QDialog
{
    Q_OBJECT

public:
    explicit Button1Popup(QWidget *parent = 0);
    ~Button1Popup();

private:
    Ui::Button1Popup *ui;
};

#endif // BUTTON1POPUP_H

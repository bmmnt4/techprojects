#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //QRect fill(0, 0, 1000, 700);
    //ui->gridLayout->setGeometry(fill);
    //ui->gridLayout->maximumSize();
    //this->showFullScreen();
    //ui->AsciiTb->copyAvailable(true);
    //ui->gridLayout->setContentsMargins(0,0,0,0);
    //ui->gridLayout->setma;
    //ui->AsciiTb->setFixedHeight(1000);
    //resize(QDesktopWidget().availableGeometry(this).size());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_decode_pressed()
{
    if (ui->BinaryTb->toPlainText() != "")
    {
        QString binary = ui->BinaryTb->toPlainText();
        binary.replace(" ", "");
        ui->AsciiTb->setText("");
        int pos = 128;
        int decimal = 0;
        QChar letter = ' ';
        bool properInput = true;
        for(int i = 0; i < binary.size(); i++)
        {
            if(binary.at(i) != '1' && binary.at(i) != '0')
            {
                properInput = false;
            }
        }
        if (binary.size() % 8 == 0 && properInput)
        {
            for (int i = 0; i < binary.size(); i++)
            {
                decimal += (binary.at(i).unicode() - 48) * pos;
                pos /= 2;
                if (pos <= 0)
                {
                    letter = decimal;
                    ui->AsciiTb->insertPlainText(letter);//letter);
                    decimal = 0;
                    pos = 128;
                }
            }
            letter = decimal;
            ui->AsciiTb->insertPlainText(letter);
        }
        else
        {
            ui->AsciiTb->setPlainText("");
            ui->BinaryTb->setPlainText("Invalid input");
        }
    }
}

void MainWindow::on_encode_pressed()
{
    if (ui->AsciiTb->toPlainText() != "")
    {
        QString words = ui->AsciiTb->toPlainText();
        ui->BinaryTb->setPlainText("");
        int multiplier = 1;
        int binary = 0;
        QChar letter;
        int decimal;
        for(int i = 0; i < words.size(); i++)
        {
            letter = words.at(i);
            decimal = letter.unicode();
            binary = 0;
            multiplier = 1;
            // place the remainder at the beginning, then divide by 2
            while (decimal > 0)
            {
                binary += decimal % 2 * multiplier;
                multiplier *= 10;
                decimal /= 2;
            }
            if (binary < 1000000)
            {
                ui->BinaryTb->insertPlainText("00" + QString::number(binary));
            }
            else
            {
                ui->BinaryTb->insertPlainText("0" + QString::number(binary));
            }
        }
    }
}

void MainWindow::on_Clear_pressed()
{
    ui->AsciiTb->setPlainText("");
    ui->BinaryTb->setPlainText("");
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_encode_pressed();

    void on_decode_pressed();

    void on_Clear_pressed();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
